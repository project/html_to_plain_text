Html to Plaintext module simply gets html code  and parse it into 
drupal_html_to_text function and return plain text. 

For admin settings please visit this page and enable parser functionality to 
regarding content type form.  After enable this functionality you will see a
fieldset in your enabled content type form.